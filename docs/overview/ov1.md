# May 2020 project overview and grant re-submission
### Data

#### Summary
- dot motion task with orthogonally manipulated perceptual and outcome uncertainty
- outcome measures: reaction times, pupil, MRI
- 30 behavioural subjects, 11 subjects in MRI (2 sessions)
- reaction times reflect both types of uncertainty, but outcome uncertainty (OU) effect is consistently weaker and somewhat inconsistent
- differences between motion and color

#### Task
- color/motion titrated to high and low coherence levels (accuracy of 65% and 90%), corresponding to high and low perceptual uncertainty in the main task
- titration is done beforehand, but adjustment is also running in the main task
- outcome uncertainty is manipulated by reward magnitude which either ranges from 40 to 60 or from 15 to 85 [it is now discrete in steps of 5 - I am not sure that's the best]
- 16 unique stimuli, repeated twice per mini block (32 trials), 8 mini blocks (256 trials overall)
<table borderwidth=0><tr>
<td><img alt="percunc" src="overview/img/perceptual_unc.png"></td>
<td><img alt="outcunc" src="overview/img/outcome_unc.png"></td>
</tr></table>

#### Trial Timing
##### Behavioural
![trial](img/experiment1.png)
##### MRI
![trialmri](img/experiment_fmri1.png)

#### Titration
##### Behavioural
<table borderwidth=0><tr>
<td><img alt="acc3" src="overview/img/acc_3.jpg"></td>
<td><img alt="acc3t" src="overview/img/acc_time_3.jpg"></td>
</tr></table>

##### MRI
<table><tr>
<td><img alt="macc3" src="overview/img/mri_acc_3.jpg"></td>
<td><img alt="macc3t" src="overview/img/mri_acc_time_3.jpg"></td>
</tr>
</table>

#### Domain differences and changes over time

 - motion perceived harder than color, but RTs are faster
 - both get faster over time  
 ![rts_over_time](img/rt_over_time.jpg)

#### RTs by uncertainties
- only correct trials included

##### Behavioural
<table>
<tr><td>Relevant/attended dimension</td><td>Irrelevant/unattended dimension</td>
<tr>
<td><img alt="rt1" src="overview/img/rts_unc.jpg"></td>
<td><img alt="rt2" src="overview/img/rts_unc_irr.jpg"></td>
</tr>
<tr >
<td colspan="2"> <img alt="rt1b" src="overview/img/rts_unc_domain.jpg"></td>
</tr>
</table>

##### MRI
<table>
<tr><td>Relevant/attended dimension</td><td>Irrelevant/unattended dimension</td>
<tr>
<td><img alt="rt1" src="overview/img/mri_rts_unc.jpg"></td>
<td><img alt="rt2" src="overview/img/mri_rts_unc_irr.jpg"></td>
</tr>
<tr >
<td colspan="2"> <img alt="rt1b" src="overview/img/mri_rts_unc_domain.jpg"></td>
</tr>
</table>

### Grant proposal
[Here](https://drive.google.com/file/d/11brZ2anQeBBPru0Flpq887wAr_oxnMxo/view?usp=sharing) is the original grant proposal.  
[Here](https://drive.google.com/open?id=1qC8XZp-IYdRGepb5VXSOwUTsuxEwlPGN) are the full responses from reviewers

#### Reviewers' comments

##### The easy-to-address

**1. What is the hypothesis about wagering? Both perceptual  and  reward  uncertainty could affect the willingness to wager but they can’t be separated from the data.**
  - we do not include wagering any more, it was proposed to take place on 20% of trials, but this would make the multivariate analysis trickier
  - instead, we ask participants **twice** in each block about their perceptual uncertainty explicitly, and at the very end of the block we ask for expected reward, minimum and maximum for each domain-feature combination (4), so the two can now be separated from the data  
  - these measures are in place **a)** to validate that people have learned reward distributions; and **b)** to inform the model   

**2. It is important to be anatomically precise,  they seem to want us to specify regions in vmPFC or OFC with more specific hypotheses considering the animal literature.**
  - we will make anatomical prediction based on Schuck et al. 2016 and maybe Muller et al. 2019 (probabilistic representation of unexpected uncertainty in the OFC)
  - I think we could be more clear about our neural hypotheses, i.e. that the different uncertainties are likely represented at different locations on their own, but the OFC needs its own representation to navigate through the task space.

**3. Number of trials per subjects is not huge, should more trials and less subjects be considered?**
  - we have increased number of trials, behavioural subject completes 256 main task trials, while in the MRI it's 384 (7 blocks, 2 on session 1 + 5 on session 2)
  - should we add power analysis?

**4. No pilot data.**
  - we do have pilot data now
  - will it be helpful to do basic MRI analyses before re-submission or is it okay to submit with behavioural data only?

**5. Budget some money for the post-doc/PI to attend conferences.**
  - done.


##### The trickier ones

**6. Design is a bit complicated and convoluted**
> "Participants must remember an abstract rule, maintain two previous trials in working memory and evaluate perceptual uncertainty.If it turns out that the mOFC/vmPFC is involved in representing state probabilistically, it is probably because the brain region needs to infer the current state based on something. Is that something working memory, an abstract rule, or uncertain past perceptual information?" <

  - we simplified the design, so that in study 1 they only look at **1** (originally 2) stimulus and in study 2 (study 1 + state uncertainty) they judge **2 stimuli** (originally 3)
  - we fixed the rule uncertainty (4th uncertainty layer) by showing the rule explicitly at the beginning of each trial
  - to address the *something*, the manipulation of the 2/3 uncertainties in itself should be sufficient to show what drives the decision - if a participant only decides based on "working memory" this means they would ignore the perceptual component and so it should be apparent from the data. Further, what *concrete* abstract rule could they use to make accurate choices? I am struggling to find one which would lead them to make accurate choices, other than considering states, perception and outcomes all together which is out manipulation.

> "I would like to suggest the applicants think carefully about the mOFC/vmPFC can be dissociated in terms of the proposed functions as the region is rather broad." <

  - yes, we can do that, I guess based on the animal literature


**7. Different uncertainties are confounded with different aspects of the task**

> "the design makes it impossible to tell apart neural uncertainty signal and signal of features of the stimulus (see Bach and Dolan, 2012)" <

  - this is only partially true, the *coherence* represents the visual aspect but the *accuracy* the level of uncertainty, these are not the same
  - they propose to show a link between neural data and behaviour - I think we could get at this using the model and the previous point  

> "Sensory uncertainty is linked to task difficulty, so the areas could reflect the amount of attentional resources

  - yes, in fact models of attention *define* attention as unexpected uncertainty (Yu and Dayan, 2005), that aside, it is a genuine confound that we can dissociate theoretically, but practically I struggle to come up with a good solution
  - some, not entirely convincing, solutions:
      a) Make assumption about the link between *unexpected uncertainty* and pupil (work of Christian Ruff, Jill O'Reilly, Peter Dayan might support that), and use physiologically-informed model to show whether the neural signal is driven by attention OR uncertainty (for example, outcome uncertainty should be fully known and thus *expected*, de-correlating attention and uncertainty to some extent)
